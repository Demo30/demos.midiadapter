﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SanfordMidi = Sanford.Multimedia.Midi;
using Demos.MusicTheory;

namespace Demos.MidiAdapter
{
    public class MidiFileAdapter
    {
        public Tone[][] Load(string filePath)
        {
            List<Tone[]> gatheredNoteSets = new List<Tone[]>();
            List<Tone> syncNotes = new List<Tone>();

            int lastTicksOffset = -1;

            SanfordMidi.Sequence sequence = new SanfordMidi.Sequence();
            sequence.Load(filePath);

            foreach(SanfordMidi.Track track in sequence)
            {
                foreach (SanfordMidi.MidiEvent midiEvent in track.Iterator())
                {
                    if (midiEvent.MidiMessage.MessageType == SanfordMidi.MessageType.Channel)
                    {
                        SanfordMidi.ChannelMessage msg = (SanfordMidi.ChannelMessage)midiEvent.MidiMessage;

                        if (msg.Command == SanfordMidi.ChannelCommand.NoteOn && msg.Data2 > 0)
                        {
                            Tone curNote = NoteProvider.Instance.GetToneByMidiIndex(msg.Data1);
                            if (midiEvent.AbsoluteTicks == lastTicksOffset)
                            {
                                if (!NoteProvider.Instance.DoesSetContainTone(syncNotes, curNote))
                                {
                                    syncNotes.Add(curNote);
                                }
                            }
                            else
                            {
                                if (syncNotes.Count > 0)
                                {
                                    gatheredNoteSets.Add(syncNotes.ToArray());
                                }
                                syncNotes.Clear();
                                syncNotes.Add(curNote);
                                lastTicksOffset = midiEvent.AbsoluteTicks;
                            }
                        }
                    }
                }

                if (syncNotes.Count > 0)
                {
                    gatheredNoteSets.Add(syncNotes.ToArray());
                    syncNotes.Clear();
                }
            }

            return gatheredNoteSets.ToArray();
        }
    }
}
