﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using SanfordMidi = Sanford.Multimedia.Midi;
using Demos.MusicTheory;

namespace Demos.MidiAdapter
{
    public class MidiDeviceAdapter
    {
        public event EventHandler<EventArgs> NoteInputChanged = null;
        public Tone[] CurrentlyActiveNotes { get { return this._currentlyActiveNotes.ToArray(); } }
        public bool SustainPedalPressed { get { return this._sustainPedalPressed; } }

        private List<Tone> _currentlyActiveNotes = new List<Tone>();
        private bool _sustainPedalPressed = false;
        private SanfordMidi.InputDevice _inputDevice = null;
        //private MidiInputNoteMapping _noteMapping = null;
        private List<SanfordMidi.InputDevice> _loadedInputDevices = new List<SanfordMidi.InputDevice>();

        public MidiDeviceAdapter()
        {
            //this._noteMapping = new MidiInputNoteMapping();
        }

        public int[] LoadInputDeviceIDs()
        {
            this._loadedInputDevices.Clear();
            List<int> deviceIds = new List<int>();

            for (int i = 0; i < SanfordMidi.InputDevice.DeviceCount; i++)
            {
                SanfordMidi.InputDevice device = new SanfordMidi.InputDevice(i);
                deviceIds.Add(device.DeviceID);
                this._loadedInputDevices.Add(device);
            }

            return deviceIds.ToArray();
        }

        public void ListenToInput(int deviceId)
        {
            SanfordMidi.InputDevice device = null;

            foreach(SanfordMidi.InputDevice curDev in this._loadedInputDevices)
            {
                if (curDev.DeviceID == deviceId)
                {
                    device = curDev;
                }
            }

            if (device == null)
            {
                throw new InvalidOperationException("Unknown device. Cannot start listening.");
            }

            this._inputDevice = device;
            this._inputDevice.ChannelMessageReceived += this.OnMessageRecieved;

            this._inputDevice.StartRecording();
        }

        public void ResetMididDeviceAdapter()
        {
            foreach(SanfordMidi.InputDevice dev in this._loadedInputDevices.ToArray())
            {
                dev.StopRecording();
                this._loadedInputDevices.Remove(dev);
                dev.Dispose();
            }
        }

        private void OnMessageRecieved(object sender, SanfordMidi.ChannelMessageEventArgs args)
        {
            int curNoteIndex = args.Message.Data1;

            Tone curNote = NoteProvider.Instance.GetToneByMidiIndex(curNoteIndex);

            if (args.Message.Command == SanfordMidi.ChannelCommand.NoteOn)
            {
                this._currentlyActiveNotes.Add(curNote);
            }
            else if (args.Message.Command == SanfordMidi.ChannelCommand.NoteOff)
            {
                if (this._currentlyActiveNotes.Contains(curNote))
                {
                    this._currentlyActiveNotes.Remove(curNote);
                }
            }

            this.RaiseNoteInputChangedEvent();
        }

        private void RaiseNoteInputChangedEvent()
        {
            if (this.NoteInputChanged != null)
            {
                this.NoteInputChanged(this, EventArgs.Empty);
            }
        }
    }
}
